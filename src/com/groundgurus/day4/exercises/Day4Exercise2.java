package com.groundgurus.day4.exercises;

import java.util.Scanner;

public class Day4Exercise2 {
    public static void main(String[] args) {
        var paragraph = "Being bounced around quickly annoyed the "
                + "disheveled taxi drivers";
        
        var input = new Scanner(System.in);
        
        System.out.print("Enter string to find: ");
        var toFind = input.next();
        
        System.out.print("Enter string to replace with: ");
        var toReplaceWith = input.next();
        
        paragraph = paragraph.replace(toFind, toReplaceWith);
        
        System.out.println(paragraph);
    }
}
