package com.groundgurus.day4.exercises;

public class Day4Exercise1 {
    public static void main(String[] args) {
        String paragraph = "Public Relations Agent Asks Sports Room, "
                + "When Do Television Disc Jockeys Fight?";
        System.out.println(paragraph.toLowerCase());
    }
}
