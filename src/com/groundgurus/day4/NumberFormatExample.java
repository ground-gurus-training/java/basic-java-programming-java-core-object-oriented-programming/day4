package com.groundgurus.day4;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatExample {
    public static void main(String[] args) {
        NumberFormat nf = NumberFormat.getInstance(Locale.ITALY);
        double price = 19877231.45;

        System.out.println(nf.format(price)); // outputs 1,231.45
    }
}
