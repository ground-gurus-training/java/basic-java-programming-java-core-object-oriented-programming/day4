package com.groundgurus.day4;

import java.io.IOException;

public class PropertiesExample {
    public static void main(String[] args) {
//        String filePath = System.getProperty("user.home");
//        System.out.println(filePath);
         var properties = System.getProperties();
         properties.list(System.out);
    }
}
