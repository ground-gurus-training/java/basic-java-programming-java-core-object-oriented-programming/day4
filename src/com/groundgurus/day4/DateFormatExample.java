package com.groundgurus.day4;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class DateFormatExample {

    public static void main(String[] args) {
        // shows the date and time of a certain locale
//        DateFormat df = DateFormat.getDateTimeInstance(
//                DateFormat.LONG, DateFormat.LONG, Locale.KOREA);
//        String today = df.format(new Date());
//        System.out.println(today);

        var today = LocalDateTime.now();
        System.out.println(today.format(DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss")));
    }
}
